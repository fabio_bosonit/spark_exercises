
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

object MNM {
  def main(args: Array[String]) {
    
    val spark = SparkSession
      .builder
      .appName("MNM")
      .getOrCreate()
    
    val mnmDf = spark.read.format("csv").option("header", "true").load("dbfs:/FileStore/shared_uploads/fabio.bianchini@bosonit.com/mnm_dataset.csv")
    

    val count_df = mnmDf
      .select("State", "Color", "Count")
      .groupBy("State", "Color")
      .agg(count("Count").alias("Total"))
      .orderBy(desc("Total"))

    count_df.show(60)

    val avg_color_df = mnmDf
      .select("State", "Color", "Count")
      .groupBy("State", "Color")
      .agg(count("Count").alias("Total"))
      .groupBy("Color")
      .agg(avg("Total").alias("Average"))
      .orderBy(asc("Average")
    )
    avg_color_df.show()
    
    val stats_df = mnmDf
      .select("State", "Color", "Count")
      .groupBy("Color", "State")
      .agg(count("Count").alias("Total"))
      .groupBy("Color")
      .agg(min("Total").alias("Min"), max("Total").alias("Max"), avg("Total").alias("Average"), sum("Total").alias("Sum"))
    stats_df.show()
  
    spark.stop()
  }
}