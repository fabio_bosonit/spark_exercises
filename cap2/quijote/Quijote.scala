// Databricks notebook source
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

def main(args: Array[String]) {
  val spark = SparkSession
      .builder
      .appName("Quijote")
      .getOrCreate()

  val quijote_df = spark.read.text("dbfs:/FileStore/shared_uploads/fabio.bianchini@bosonit.com/el_quijote-1.txt")

  // Muestra del dataframe con un número customizado de filas
  quijote_df.show(30)

  // Muestra del dataframe con el parámetro truncate establecido como falso (muestra el contenido completo de las filas)
  quijote_df.show(30, false)

  // Primera fila del df
  quijote_df.head()
  quijote_df.first()

  // Última fila del df
  quijote_df.tail(1)
  
  
  
  // La diferencia entre head y tail es que ambos devuelven un número determinado de filas, head partiendo desde el principio y tail desde el final del dataframe. El número por defecto de filas que se devuelve en el método head es 1. First solo tiene la posibilidad de devolver la primera línea del dataframe.

  spark.stop()
}

// COMMAND ----------


