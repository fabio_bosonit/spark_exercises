
import org.apache.spark.sql.SparkSession

val spark = SparkSession
        .builder
        .appName("SparkSQLExampleApp")
        .getOrCreate()


val schema = "date STRING, delay INT, distance INT, origin STRING, destination STRING"
val csv_file = "/databricks-datasets/learning-spark-v2/flights/departuredelays.csv"
val df = spark.read.format("csv")
     .option("header", true)
     .option("schema", schema)
     .load(csv_file)

df.createOrReplaceTempView("us_delay_flights")

// Qué vuelos recorrieron más de 1000 millas
spark.sql("""SELECT distance, origin, destination 
    FROM us_delay_flights 
    WHERE distance > 1000 
    ORDER BY distance DESC""").show(10)

// Qué vuelos con origen San Francisco (SFO) y destino Chicago (ORD) tuvieron un retraso superior a 2 horas
spark.sql("""SELECT date, delay, origin, destination
    FROM us_delay_flights
    WHERE delay > 120 AND origin = 'SFO' AND destination = 'ORD'
    ORDER BY delay DESC
    """).show(10)

// Etiquetas legibles
spark.sql("""SELECT delay, origin, destination,
    CASE
        WHEN delay > 360 THEN 'Retraso muy grande'
        WHEN delay < 360 AND delay > 120 THEN 'Retraso grande'
        WHEN delay < 120 AND delay > 60 THEN 'Retraso moderado'
        WHEN delay < 60 AND delay > 0 THEN 'Retraso corto'
        WHEN delay = 0 THEN 'Sin retraso'
        ELSE 'Adelantado'
    END AS flight_delays
    FROM us_delay_flights
    ORDER BY origin, delay DESC
""").show(10)

// API de DF
import org.apache.spark.sql.functions.{desc}
df
 .select("date", "delay", "origin", "destination")
 .where("delay > 120 AND origin = 'SFO' AND destination = 'ORD'")
 .orderBy(desc("delay")).show(10)


// Tablas administradas vs tablas no administradas
// Existen dos tipos de tablas en Spark: administradas *(managed)* y no administradas *(unmanaged)*.
// 
// Administradas
// Spark administra tanto los metadatos como los propios datos en el almacenamiento (ya sea el sistema de ficheros local, HDFS, Amazon S3...).
// Los comandos SQL como DROP TABLE borra tanto los metadatos como los datos.
// No administradas
// Spark solo se encarga de administrar los metadatos, mientras que los propios datos debemos administrarlos nosotros en una fuente de datos como Cassandra o ficheros. Los comandos SQL como DROP TABLE solo borran los metadatos.
// Crear bases de datos y tablas SQL
// Las tablas pertenecen a una base de datos. Por defecto, Spark crea las tablas en la base de datos *default*. Sin embargo, podemos crear y usar nuestras propias bases de datos, lanzando las siguientes queries:
spark.sql("DROP DATABASE IF EXISTS learn_spark_db")
spark.sql("CREATE DATABASE learn_spark_db")
spark.sql("USE learn_spark_db")


// Crear una tabla administrada
// Para crear una tabla administrada, escribimos la query que define la tabla:
spark.sql("CREATE TABLE managed_us_delay_flights_tbl (date STRING, delay INT, distance INT, origin STRING, destination STRING)")


val schema = "date STRING, delay INT, distance INT, origin STRING, destination STRING"
val flights_df = spark.read.format("csv").schema(schema).load("/databricks-datasets/learning-spark-v2/flights/departuredelays.csv")
flights_df.write.mode("overwrite").saveAsTable("managed_us_delay_flights_tbl")


// Crear una tabla no administrada
// Podemos crear una tabla no administrada usando como data source, por ejemplo, un fichero CSV:
spark.sql("""CREATE TABLE unmanaged_us_delay_flights_tbl(date STRING, delay INT,
 distance INT, origin STRING, destination STRING)
 USING csv OPTIONS (PATH
 '/databricks-datasets/learning-spark-v2/flights/departuredelays.csv')""")


// API de DF:
flights_df
 .write
 .option("path", "/tmp/data/us_flights_delay")
 .saveAsTable("unmanaged_delay_flights_tbl_scala")


// Crear vistas
// Creamos una vista con los vuelos con origen en San Francisco (SFO):
// Como vista global (prefijo global_temp antes del nombre de la tabla):
spark.sql("""CREATE OR REPLACE GLOBAL TEMP VIEW us_origin_airport_SFO_global_tmp_view AS
 SELECT date, delay, origin, destination from unmanaged_us_delay_flights_tbl 
     WHERE origin = 'SFO'""")
spark.read.table("global_temp.us_origin_airport_SFO_global_tmp_view").show(5)


// Como vistas normales:
spark.sql("""CREATE OR REPLACE TEMP VIEW us_origin_airport_SFO_tmp_view AS
 SELECT date, delay, origin, destination from unmanaged_us_delay_flights_tbl 
     WHERE origin = 'SFO'""")
spark.read.table("us_origin_airport_SFO_tmp_view").show(5)


// Creamos primero un nuevo dataframe con los campos que nos interesan y lo convertimos luego en una vista
val df_jfk = spark.sql("SELECT date, delay, origin, destination FROM us_delay_flights WHERE origin = 'JFK'")
df_jfk.createOrReplaceGlobalTempView("us_origin_airport_JFK_global_tmp_view")
df_jfk.createOrReplaceTempView("us_origin_airport_JFK_tmp_view")


// Metadatos en Spark
// Listar bases de datos:
spark.catalog.listDatabases()


// Listar tablas:
spark.catalog.listTables()


// Listar columnas de una tabla:
spark.catalog.listColumns("managed_us_delay_flights_tbl")


// Leer tablas como DFs
// Podemos leer tablas SQL y convertirlas a DF a través de la función table() o de la función sql("SELECT...")
val df_1 = spark.table("managed_us_delay_flights_tbl")
val df_2 = spark.sql("SELECT * FROM managed_us_delay_flights_tbl")



// Parquet
val file = """/databricks-datasets/learning-spark-v2/flights/summary-data/parquet/2010-summary.parquet/"""
val df = spark.read.format("parquet").load(file)
df.show(5)


// Parquet como SQL
spark.sql("""CREATE OR REPLACE TEMPORARY VIEW us_delay_flights_tbl
USING parquet
OPTIONS (
path "/databricks-datasets/learning-spark-v2/flights/summary-data/parquet/2010-summary.parquet/" )""")
spark.table("us_delay_flights_tbl").show(5)


// Escribir DF en Parquet
df.write.format("parquet")
.mode("overwrite")
.option("compression", "snappy")
.save("/tmp/data/parquet/df_parquet")


// JSON
// Leer JSON como DF
val file = "/databricks-datasets/learning-spark-v2/flights/summary-data/json/*"
val df = spark.read.format("json").load(file)
df.show(5)


// Leer JSON como una tabla SQL
spark.sql("""CREATE OR REPLACE TEMPORARY VIEW us_delay_flights_tbl
USING json
OPTIONS (
path "/databricks-datasets/learning-spark-v2/flights/summary-data/json/*"
)""")
spark.table("us_delay_flights_tbl").show(5)


// Escribir DF en JSON
df.write.format("json")
.mode("overwrite")
.option("compression", "snappy")
.save("/tmp/data/json/df_json")



// CSV
val file = "/databricks-datasets/learning-spark-v2/flights/summary-data/csv/*"
val schema = "DEST_COUNTRY_NAME STRING, ORIGIN_COUNTRY_NAME STRING, count INT"
val df = spark.read.format("csv")
.option("header", "true")
.option("nullValue", "") // Remplaza los valores nulos con quotes
.schema(schema)
.load(file)
df.show(5)


// Leer CSV como una tabla SQL
spark.sql("""CREATE OR REPLACE TEMPORARY VIEW us_delay_flights_tbl
USING csv
OPTIONS (
path "/databricks-datasets/learning-spark-v2/flights/summary-data/csv/*",
header "true",
inferSchema "true",
mode "FAILFAST"
)""")
spark.table("us_delay_flights_tbl").show(5)


// Escribir DF como CSV
df.write.format("csv")
 .mode("overwrite")
 .save("/tmp/data/csv/df_csv")


// Imágenes
// Leer una imagen como DF
import org.apache.spark.ml.source.image
val image_dir = "/databricks-datasets/learning-spark-v2/cctvVideos/train_images/"
val images_df = spark.read.format("image").load(image_dir)
images_df.printSchema()


images_df.select("image.height", "image.width", "image.mode", "label").show(5, false)


// Archivos binarios
// Leer un fichero binario como un DF
val path = "/databricks-datasets/learning-spark-v2/cctvVideos/train_images/"
val binary_files_df = spark.read.format("binaryFile")
.load(path)
binary_files_df.show(5)
