import org.apache.spark.sql.SparkSession

val spark = SparkSession
        .builder
        .appName("capitulo5")
        .getOrCreate()

val biquadrates = (num: Long) => {
  scala.math.pow(num, 4)
}

spark.udf.register("biquadrates", biquadrates)


spark.range(1, 9).createOrReplaceTempView("udf_test")
spark.sql("SELECT id, biquadrates(id) FROM udf_test").show()


// Funciones de orden superior
import org.apache.spark.sql._
import org.apache.spark.sql.types._

val arrayData = Seq(
  Row(1, List(1, 2, 3)),
  Row(2, List(2, 3, 4)),
  Row(3, List(3, 4, 5))
)

val arraySchema = new StructType()
  .add("id", IntegerType)
  .add("values", ArrayType(IntegerType))

val df = spark.createDataFrame(spark.sparkContext.parallelize(arrayData), arraySchema)
df.createOrReplaceTempView("table")
df.printSchema()
df.show()


// Explode y Collect
spark.sql("""
SELECT id, collect_list(value + 1) AS values
    FROM (SELECT id, EXPLODE(values) AS value
    FROM table) x
GROUP BY id
""").show()


// UDF
def addOne(values: Seq[Int]): Seq[Int] = {
    values.map(value => value + 1)
}

val plusOneInt = spark.udf.register("plusOneInt", addOne(_: Seq[Int]): Seq[Int])
spark.sql("SELECT id, plusOneInt(values) AS values FROM table").show()


// Built-in Functions 
val t1 = Array(35, 36, 32, 30, 40, 42, 38)
val t2 = Array(31, 32, 34, 55, 56)
val tC = Seq(t1, t2).toDF("celsius")
tC.createOrReplaceTempView("tC")
tC.show(false)


// transform()
// Esta función toma como parámetros un array y una función lambda y retorna un nuevo array que surge de aplicar esta función a cada elemento del array.
spark.sql("""
SELECT celsius, transform(celsius, t -> ((t * 9) div 5) + 32) as fahrenheit FROM tC
""").show(false)


// filter()
// La función filter produce un nuevo array que contiene solo los elementos del array de entrada para los que la función se evalúa a True.
spark.sql("""
SELECT celsius,
filter(celsius, t -> t > 38) as high
FROM tC
""").show(false)


// exists()
// La función exists retorna True si existe algún elemento del array para el que la función lambda se evalúe a True.
spark.sql("""SELECT celsius,
    exists(celsius, t -> t = 38) as threshold
FROM tC
""").show(false)


// reduce()
// La función reduce() reduce los elementos del array a un único valor a través de unirlos en un buffer B mediante la función function<B, T, B> y finalmente aplicando la función function<B, R> al buffer final.
spark.sql("""
SELECT celsius,
    reduce(
        celsius,
        0,
        (t, acc) -> t + acc,
        acc -> (acc div size(celsius) * 9 div 5) + 32
    ) as avgFahrenheit
FROM tC
""").show(false)


// Operaciones con DataFrames y SparkSQL
import org.apache.spark.sql.functions._

val delaysPath = "/databricks-datasets/learning-spark-v2/flights/departuredelays.csv"
val airportsPath = "/databricks-datasets/learning-spark-v2/flights/airport-codes-na.txt"

val airports = spark
  .read
  .options(
    Map(
      "header" -> "true", 
      "inferSchema" ->  "true", 
      "sep" -> "\t"))
  .csv(airportsPath)

airports.createOrReplaceTempView("airports_na")

val delays = spark
  .read
  .option("header","true")
  .csv(delaysPath)
  .withColumn("delay", expr("CAST(delay as INT) as delay"))
  .withColumn("distance", expr("CAST(distance as INT) as distance"))

delays.createOrReplaceTempView("departureDelays")

val seaSfoDF = delays
  .filter(
    expr("""
         origin == 'SEA' AND 
         destination == 'SFO' AND 
         date like '01010%' AND delay > 0
         """))

seaSfoDF.createOrReplaceTempView("sea_sfo")


// Unions
val union = delays.union(seaSfoDF)
union.createOrReplaceTempView("union")
union.filter(expr("""
            origin == 'SEA' AND 
            destination == 'SFO' AND 
            date like '01010%' AND 
            delay > 0""")).show()


// Joins
seaSfoDF.join(
    airports.as("air"),
    $"air.IATA" === $"origin"
).select("City", "State", "date", "delay", "distance", "destination").show()


// Windowing
val departureDelaysWindow = delays
    .select("origin", "destination", "delay")
    .where(col("origin").isin("SEA", "SFO", "JFK"))
    .where(col("destination").isin("SEA", "SFO", "JFK", "DEN", "ORD", "LAX", "ATL"))
    .groupBy("origin", "destination")
    .agg(sum("delay").alias("TotalDelays"))
departureDelaysWindow.show()


import org.apache.spark.sql.expressions.Window
val windowSpec = Window.partitionBy("origin").orderBy(desc("TotalDelays"))
departureDelaysWindow
    .select("origin", "destination", "TotalDelays")
    .withColumn("rank", dense_rank().over(windowSpec))
    .filter(col("rank") <= 3)
.show()


// Modificaciones
// Añadir columnas
val statusDF = seaSfoDF.withColumn("status",
  expr("CASE WHEN delay <= 10 THEN 'On-time' ELSE 'Delayed' END"))
statusDF.show()


// Quitar columnas
statusDF.drop("delay").show()


// Renombrar columnas
statusDF.withColumnRenamed("status", "flight_status").show()


// Pivotar
val monthDF = delays
            .select($"destination", expr("CAST(SUBSTRING(date, 0, 2) AS INT) AS month"), $"delay")
            .where($"origin" === "SEA")
monthDF.show()


monthDF
    .groupBy("destination")
    .pivot("month")
    .agg(avg("delay").alias("avg"),
         max("delay").alias("max"))
    .orderBy("destination")
.show()