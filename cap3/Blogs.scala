
import org.apache.spark.sql.types._
import org.apache.spark.sql.functions._

val schema = StructType(Array(StructField("Id", IntegerType, false),
  StructField("First", StringType, false),
  StructField("Last", StringType, false),
  StructField("Url", StringType, false),
  StructField("Published", StringType, false),
  StructField("Hits", IntegerType, false),
  StructField("Campaigns", ArrayType(StringType), false)));

val jsonFile = "/databricks-datasets/learning-spark-v2/blogs.json";
val blogsDF = spark.read.schema(schema).json(jsonFile);
blogsDF.printSchema();



blogsDF.columns

blogsDF.select(expr("Hits * 2")).show(5)


blogsDF.select(col("Hits")*2).show(5);
blogsDF.select($"Hits"*2).show(5);


val bigHittersBlogsDF = blogsDF.withColumn("Big Hitters", $"Hits" > 10000);
bigHittersBlogsDF.show(5);

